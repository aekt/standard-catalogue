import { setBookStarred } from "./api";
import { el } from "./utils";
import { Book } from "./types";

export function elError(error: string) {
  return el("span", {}, error);
}

export function elLoading() {
  return el("div", { id: "loading" }, "Loading...");
}

export function elBookList(books: Book[]) {
  return el("div", {}, ...books.map(elBookItem));
}

function elBookItem(book: Book) {
  const bookItem = el(
    "div",
    { class: "item" },
    elStar(book),
    el("div", { class: "item__title" }, book.title),
    el("div", { class: "item__author" }, book.author),
    el("details", {}, el("summary", {}, "Summary"), book.summary),
    el("details", {}, el("summary", {}, "Content"), book.content),
    el(
      "div",
      { class: "item__links" },
      ...book.links.map(link =>
        el(
          "div",
          { class: "link" },
          el("a", { class: "link", href: link.url }, link.format)
        )
      )
    )
  );
  return bookItem;
}

function elStar(book: Book) {
  const icon = el("span", { class: book.starred ? "starred" : "" }, "✱");
  const star = el("div", { class: "star" }, icon);
  star.addEventListener("click", () => {
    book.starred = !book.starred;
    icon.className = book.starred ? "starred" : "";
    setBookStarred(book.id, book.starred);
  });
  return star;
}
