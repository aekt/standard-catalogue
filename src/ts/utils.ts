import { Book, Link } from "./types";

export function el(tag: string, attributes, ...children) {
  let node = document.createElement(tag);
  for (let attribute in attributes) {
    node.setAttribute(attribute, attributes[attribute]);
  }
  for (let child of children) {
    if (typeof child === "string") {
      node.appendChild(document.createTextNode(child));
    } else {
      node.appendChild(child);
    }
  }
  return node;
}

export function textOf(element, ...tags) {
  const [tag, ...more] = tags;
  if (tag === undefined) return element.textContent;
  return textOf(element.getElementsByTagName(tag)[0], ...more);
}
