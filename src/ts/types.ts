type Format = "epub" | "epub3" | "kepub" | "azw3";

export interface Link {
  format: Format;
  url: string;
}

export interface Book {
  id: string;
  starred: boolean;
  published: Date;
  updated: Date;
  title: string;
  author: string;
  summary: string;
  content: string;
  links: Link[];
}

export type BookSortableAttribute = "title" | "author" | "updated";
