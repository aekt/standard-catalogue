import { Book, Link } from "./types";
import { textOf } from "./utils";

export function setBookStarred(bookId, starred) {
  let starredBooks = JSON.parse(localStorage.starredBooks || "[]");
  localStorage.starredBooks = JSON.stringify(
    starred
      ? starredBooks.concat([bookId])
      : starredBooks.filter(id => id !== bookId)
  );
}

function isStarred(bookId): boolean {
  const starredBooks = JSON.parse(localStorage.starredBooks || "[]");
  return starredBooks.includes(bookId);
}

export function fetchBooks() {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          try {
            const books = parseBooks(xhr.responseXML);
            resolve(books);
          } catch (error) {
            reject(error);
          }
        } else {
          reject(xhr.statusText);
        }
      }
    };
    xhr.open("GET", "https://standardebooks.org/opds/all");
    xhr.send();
  });
}

function parseBooks(element): Book[] {
  const feed = element.getElementsByTagName("feed")[0];
  const entries = feed.getElementsByTagName("entry");
  return Array.from(entries).map(entry => {
    const id = textOf(entry, "id");
    return {
      id,
      starred: isStarred(id),
      published: new Date(textOf(entry, "published")),
      updated: new Date(textOf(entry, "updated")),
      title: textOf(entry, "title"),
      author: textOf(entry, "author", "name"),
      summary: textOf(entry, "summary"),
      content: textOf(entry, "content"),
      links: parseLinks(entry)
    };
  });
}

function parseLinks(element): Link[] {
  const domainPrefix = "https://standardebooks.org";
  const formatOf = {
    epub: "epub",
    epub3: "epub3",
    "kepub.epub": "kepub",
    azw3: "azw3"
  };

  let links: Link[] = [];
  Array.from(element.getElementsByTagName("link")).forEach((l: HTMLElement) => {
    const href = l.getAttribute("href");
    const matched = href.match(/\.(.*)$/);
    if (!matched || matched.length < 2) {
      return;
    }
    const ext = matched[1];
    const url = domainPrefix + href;
    if (ext in formatOf) {
      links.push({ format: formatOf[ext], url });
    }
  });
  return links;
}
