import { fetchBooks } from "./api";
import { Book, BookSortableAttribute } from "./types";
import { elBookList, elError, elLoading } from "./elements";

const menu = document.querySelector("#menu");
const menuBackground = document.querySelector("#menu__background");
const navMenu = document.querySelector("#nav__menu");
const showAll = document.querySelector("#show__all");
const showStarred = document.querySelector("#show__starred");
const sortOptions = document.querySelectorAll("#sort__options > li");
let selectedSortOption = document.querySelector("#sort__options > li.selected");
const listing = document.querySelector("#listing");

let books: Book[] = [];
let list: Book[] = [];
let attr: BookSortableAttribute = "updated";

function show(element, dom) {
  while (dom.firstChild) {
    dom.firstChild.remove();
  }
  dom.appendChild(element);
}

function showMenu() {
  menu.className = "navigating";
  document.body.className = "freezed";
}

function hideMenu() {
  menu.className = "";
  document.body.className = "";
}

function listBooks() {
  let order: (a: Book, b: Book) => number;
  switch (attr) {
    case "updated":
      order = (a, b) => (a.updated > b.updated ? -1 : 1);
      break;
    default:
      order = (a, b) => (a[attr] < b[attr] ? -1 : 1);
      break;
  }
  show(elBookList(list.slice().sort(order)), listing);
  window.scrollTo(0, 0);
}

navMenu.addEventListener("click", showMenu);
menuBackground.addEventListener("click", hideMenu);

showAll.addEventListener("click", () => {
  hideMenu();
  list = books.slice();
  listBooks();
});

showStarred.addEventListener("click", () => {
  hideMenu();
  list = books.filter(book => book.starred);
  listBooks();
});

Array.from(sortOptions).forEach(option => {
  option.addEventListener("click", () => {
    selectedSortOption.classList.toggle("selected");
    option.classList.toggle("selected");
    selectedSortOption = option;
    hideMenu();
    attr = option.id as BookSortableAttribute;
    listBooks();
  });
});

(async () => {
  show(elLoading(), listing);
  try {
    books = (await fetchBooks()) as Book[];
  } catch (error) {
    show(elError(error), listing);
  }
  list = books.slice();
  listBooks();
})();
